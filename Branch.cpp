#include"Branch.h"

Branch::Branch(Branch* inParent, int inGen, std::string inName) :
    parent(inParent), gen(inGen), name(inName) {

    ++inGen;

    if (inGen < maxGen) {
        int n = (inGen == 1) ? 3 + rand() % 2 : 2 + rand() % 1;
        for (int i = 0; i < n; ++i) {
            std::cout << "Please input the name of the elv from the " << i + 1 <<
                " branch (" << inGen << " level)\n";
            std::string temp;
            std::cin >> temp;
            children.push_back(new Branch(this, inGen, temp));
        }
    }
}
Branch::~Branch() {
    std::cout << "tree deleted\n";
}

Branch* Branch::GetChildAt(const int& i) {
    return children[i];
}
const int& Branch::GetChildSize() const {
    return children.size();
}
const std::string& Branch::GetName() const {
    return name;
}
