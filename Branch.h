#pragma once
#include <iostream>
#include <vector>
#include <string>

class Branch {
private:
    Branch* parent;
    std::vector <Branch*> children;
    std::string name;
    int gen;  // number of generation
    int maxGen = 3;  // maximum amount of generations

public:
    Branch(Branch* inParent, int inGen, std::string inName);
    ~Branch();
    Branch* GetChildAt(const int& i);
    const int& GetChildSize() const;
    const std::string& GetName() const;
};
