/*Задание 1. Реализация деревни эльфов

Что нужно сделать

Лесные эльфы расположили свою деревню в лесу, прямо на деревьях.
Нужно расселить эльфов по веткам деревьев, а затем подсчитать
общее количество соседей определённого эльфа.

Всего в лесу пять деревьев, на каждом из деревьев 3–5 основных
больших ветвей. На каждой из них расположены ещё по 2-3 средние
ветки. Дома эльфов построены только на больших и средних ветках.

В начале программы пользователь размещает в каждом доме по одному
эльфу. Делается это с помощью последовательного перебора всех
имеющихся домов и запроса имени заселяемого эльфа через
стандартный ввод. Если было введено None в качестве имени, то дом
пропускается и не заселяется вообще никем.

После этого требуется найти определённого эльфа по имени. Имя
искомого эльфа вводится через стандартный ввод. Для данного эльфа,
если таковой был найден, требуется вывести общее количество эльфов,
живущих вместе с ним на одной большой ветви.

Советы и рекомендации

Реализуйте генерацию деревьев с помощью кода, используя случайные
числа. Для организации структуры деревьев используйте указатель this.
*/

#include <iostream>
#include <vector>
#include "Branch.h"


int main() {

    const int treeNumb = 5;  // number of trees
    int gen = 0;  // generation variable 
    // tree initialisation
    Branch* tree[treeNumb];
    for (int i = 0; i < treeNumb; ++i) {
        tree[i] = new Branch(nullptr, gen, "None");
    }
    // finding elf
    std::cout << "Find elf? What's his name?\n";
    std::string temp;
    std::cin >> temp;
    int findOnBranch = 0;
    int findOnTree = -1;

    for (int i = 0; i < treeNumb; ++i) {
        for (int j = 0; j < tree[i]->GetChildSize(); ++j) {
            
            if (temp == tree[i]->GetChildAt(j)->GetName()) {
                // found on a big branch
                findOnTree = i; findOnBranch = j;
            }

            for (int k = 0; k < tree[i]->GetChildAt(j)->GetChildSize(); ++k) {

                if (temp == tree[i]->GetChildAt(j)->GetChildAt(k)->GetName()) {
                    // found on a ordinary branch
                    findOnTree = i; findOnBranch = j;
                }

            }
        }
    }
    // counting neighbours
    if (findOnTree >= 0) {  // we have a match
        int neighbours = 0;
        // looking thru ordinary branches
        for (int n = 0; n < tree[findOnTree]->GetChildAt(findOnBranch)->GetChildSize(); ++n) {
            if (tree[findOnTree]->GetChildAt(findOnBranch)->GetChildAt(n)->
                GetName() != "None") ++neighbours;
        }
        // checking big branch
        if (tree[findOnTree]->GetChildAt(findOnBranch)->
            GetName()!="None") ++neighbours;
        std::cout << temp << " have " << neighbours - 1 << " neighbours\n";
        // found elf is not included
    }
    //delete trees
    for (int i = 0; i < treeNumb; ++i) {
        delete tree[i];
    }
}


